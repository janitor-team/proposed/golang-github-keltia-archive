module github.com/keltia/archive

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/proglottis/gpgme v0.0.0-20181127053519-3b0be0916cd5
	github.com/stretchr/testify v1.3.0
)
